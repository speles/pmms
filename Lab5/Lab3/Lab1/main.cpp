#include <Windows.h>
#include <gl\GL.h>
#include "GL\glut.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include "Textures.h"

using namespace std;

float Y_ARROW_WIDTH = 2, Y_ARROW_LENGTH = 5;
float X_ARROW_WIDTH = 2, X_ARROW_LENGTH = 5;
float areaWidth = X_ARROW_LENGTH + Y_ARROW_WIDTH + 50;
float areaHeight = Y_ARROW_LENGTH + X_ARROW_WIDTH + 50;
int windowWidth, windowHeight;

float planetAngle = 0, sunAngle = 0, planetAngle2 = 0;
bool pause = true;

double a = 2;

GLuint moonTexture;
GLuint sunTexture;

GLfloat diffuseMaterial[] = { 1.0, 0.6, 0.0, 1.0 };
GLfloat whiteSpecularMaterial[] = { 1.0, 1.0, 1.0 };
GLfloat greenEmissiveMaterial[] = { 0.0, 1.0, 0.0 };

GLfloat whiteSpecularLight[] = { 1.0, 0.6, 0.0 };
GLfloat blackAmbientLight[] = { 0.0, 0.0, 0.0 };
GLfloat diffuseLight[] = { 1.0, 1.0, 1.0, 1.0 };

GLfloat pos[] = { 0.0, 0.0, 0.0, 1.0};

GLfloat blankMaterial[] = { 0.0, 0.0, 0.0 }; 
GLfloat mShininess[] = { 255 };

float func(float x)
{
	return x * a;
}

void init(void)
{
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_POSITION, pos);
	glAlphaFunc(GL_GREATER, 0);

	glClearColor(0, 0, 0, 1);
	glEnable(GL_DEPTH_TEST);

	moonTexture = loadTexture("moon.tga");
	sunTexture = loadTexture("sun.tga");
}

void drawShapes(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3d(1, 1, 1);

	GLUquadricObj *quadratic = gluNewQuadric();
	gluQuadricNormals(quadratic, GLU_SMOOTH);
	gluQuadricTexture(quadratic, GL_TRUE);

	glEnable(GL_TEXTURE_2D);


	glLoadIdentity();
	glRotatef(sunAngle, 0, 0, 1);

	glBindTexture(GL_TEXTURE_2D, sunTexture);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, diffuseMaterial);
	gluSphere(quadratic, 150, 100, 100);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, blankMaterial);

	glLoadIdentity();

	glRotatef(planetAngle, 0, 0, 1);
	glTranslatef(-500, -500, 0);
	glRotatef(planetAngle2, 0, 0, 1);

	glBindTexture(GL_TEXTURE_2D, moonTexture);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuseMaterial);
	gluSphere(quadratic, 75, 100, 100);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, blankMaterial);

	glLoadIdentity();

	glFlush();
}

void winReshapeFcn(GLint width, GLint height)
{
	windowWidth = width;
	windowHeight = height;

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (float)width / (float)height, 1, 20000);
	gluLookAt(0, 0, 2000, 0, 0, 0, 0, 1, 0);

	glRotatef(-50.0f, 1, 0, 0);
	glMatrixMode(GL_MODELVIEW);
}

void timerFunc(int timerId)
{
	if (timerId == 1)
	{
		if (!pause)
		{
			planetAngle += 0.5;
			planetAngle2 += 0.5;
			sunAngle -= 0.1;
		}
	}
	glutTimerFunc(17, timerFunc, timerId);
}

void idleFunc()
{
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y)
{
	if (key == VK_SPACE) pause = !pause;
	if (key == VK_BACK)
	{
		sunAngle = 0;
		planetAngle = 0;
		planetAngle2 = 0;
	}
}

void main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_MULTISAMPLE | GLUT_DEPTH);
	glutInitWindowPosition(300, 0);
	glutInitWindowSize(700, 700);
	glutCreateWindow("����������� ������ #5");
	init();

	glutTimerFunc(17, timerFunc, 1);

	glutIdleFunc(idleFunc);
	glutDisplayFunc(drawShapes);
	glutReshapeFunc(winReshapeFcn);
	glutKeyboardFunc(keyboardFunc);
	glutMainLoop();
}