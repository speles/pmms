﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Random rand = new Random();

        private void MoveToRandomSide(Ellipse ellipse)
        {
            double xPos = rand.NextDouble();
            double yPos = rand.NextDouble();

            int currSide = -1;

            if ((double)ellipse.GetValue(Canvas.LeftProperty) <= 10) currSide = 0;
            if ((double)ellipse.GetValue(Canvas.LeftProperty) >= (Width - ellipse.Width - 10)) currSide = 1;

            if ((double)ellipse.GetValue(Canvas.TopProperty) <= 10) currSide = 2;
            if ((double)ellipse.GetValue(Canvas.TopProperty) >= (Height - ellipse.Height - 10)) currSide = 3;

            int side;
            do
            {
                side = rand.Next(4);
            } while (side == currSide);
            if (side == 0) xPos = 0; //left
            if (side == 1) xPos = 1; //right
            if (side == 2) yPos = 0; //top;
            if (side == 3) yPos = 1; //bottom

            Storyboard sb = new Storyboard();

            DoubleAnimation xAnimation = new DoubleAnimation();
            DoubleAnimation yAnimation = new DoubleAnimation();

            xAnimation.From = (double)ellipse.GetValue(Canvas.LeftProperty);
            xAnimation.To = xPos * (Width - ellipse.Width);

            yAnimation.From = (double)ellipse.GetValue(Canvas.TopProperty);
            yAnimation.To = yPos * (Height - ellipse.Height);

            TimeSpan time = TimeSpan.FromSeconds(rand.NextDouble() + 0.5);
            xAnimation.Duration = time;
            yAnimation.Duration = time;

            Storyboard.SetTargetProperty(xAnimation, new PropertyPath("(Canvas.Left)"));
            Storyboard.SetTargetProperty(yAnimation, new PropertyPath("(Canvas.Top)"));

            sb.Children.Add(xAnimation);
            sb.Children.Add(yAnimation);

            sb.Completed += (o, e) => OnAnimCompleted(ellipse);
            ellipse.BeginStoryboard(sb);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            MoveToRandomSide(Ellipse1);
        }

        private Brush PickBrush()
        {
            Brush result = Brushes.Transparent;
            Type brushesType = typeof(Brushes);
            PropertyInfo[] properties = brushesType.GetProperties();
            int random = rand.Next(properties.Length);
            result = (Brush)properties[random].GetValue(null, null);
            return result;
        }

        private void OnAnimCompleted(Ellipse ellipse)
        {
            if (ellipse.Width <= 20)
            {
                MoveToRandomSide(ellipse);
            } else
            {
                myCanvas.Children.Remove(ellipse);
                for (int i = 0; i < 2; i++)
                {
                    Ellipse newEllipse = new Ellipse();
                    newEllipse.SnapsToDevicePixels = false;
                    newEllipse.Fill = PickBrush();
                    newEllipse.Width = ellipse.Width / 1.3;
                    newEllipse.Height = ellipse.Height / 1.3;
                    newEllipse.SetValue(Canvas.LeftProperty, (double)ellipse.GetValue(Canvas.LeftProperty));
                    newEllipse.SetValue(Canvas.TopProperty, (double)ellipse.GetValue(Canvas.TopProperty));
                    myCanvas.Children.Add(newEllipse);
                    MoveToRandomSide(newEllipse);
                }
            }
        }
    }
}
