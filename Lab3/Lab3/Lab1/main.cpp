#include <Windows.h>
#include <gl\GL.h>
#include "GL\glut.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

float Y_ARROW_WIDTH = 2, Y_ARROW_LENGTH = 5;
float X_ARROW_WIDTH = 2, X_ARROW_LENGTH = 5;
float areaWidth = X_ARROW_LENGTH + Y_ARROW_WIDTH + 50;
float areaHeight = Y_ARROW_LENGTH + X_ARROW_WIDTH + 50;
int windowWidth, windowHeight;

float angle = 0;
bool pause = true;

double a = 2;

float func(float x)
{
	return x * a;
}

void init(void)
{
	glClearColor(1, 1, 1, 1);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-areaWidth, areaWidth, -areaHeight, areaHeight);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void drawString(float x, float y, string text, void *font)
{
	glRasterPos2f(x, y);
	for (char c : text)
		glutBitmapCharacter(font, c);
}



void drawShapes(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(angle, 0, 0, 1);

	glColor3b(0, 0, 0);
	glLineWidth(1);
	glBegin(GL_LINES);
	{
		//y axis
		glVertex2f(0, -areaHeight);
		glVertex2f(0, areaHeight);

		//y axis arrow
		glVertex2f(0, areaHeight);
		glVertex2f(-Y_ARROW_WIDTH, areaHeight - Y_ARROW_LENGTH);

		glVertex2f(0, areaHeight);
		glVertex2f(Y_ARROW_WIDTH, areaHeight - Y_ARROW_LENGTH);

		//x axis
		glVertex2f(areaWidth, 0);
		glVertex2f(-areaWidth, 0);

		//x axis arrow
		glVertex2f(areaWidth, 0);
		glVertex2f(areaWidth - X_ARROW_LENGTH, X_ARROW_WIDTH);

		glVertex2f(areaWidth, 0);
		glVertex2f(areaWidth - X_ARROW_LENGTH, -X_ARROW_WIDTH);
	}
	glEnd();

	drawString(areaWidth - X_ARROW_LENGTH / 1.5, -X_ARROW_WIDTH * 4, "x", GLUT_BITMAP_TIMES_ROMAN_24);

	drawString(-Y_ARROW_WIDTH * 4, areaHeight - Y_ARROW_LENGTH, "y", GLUT_BITMAP_TIMES_ROMAN_24);

	//x axis lines
	for (int x = -50; x <= 50; x += 10)
	{
		if (x == 0) continue;
		glBegin(GL_LINES);
		{
			glVertex2f(x, X_ARROW_WIDTH);
			glVertex2f(x, -X_ARROW_WIDTH);
		}
		glEnd();

		drawString(x - 0.1, -X_ARROW_WIDTH * 3, to_string(x), GLUT_BITMAP_HELVETICA_12);
	}

	//y axis lines
	for (int y = -50; y <= 50; y += 10)
	{
		if (y == 0) continue;
		glBegin(GL_LINES);
			glVertex2f(Y_ARROW_WIDTH, y);
			glVertex2f(-Y_ARROW_WIDTH, y);
		glEnd();

		drawString(Y_ARROW_WIDTH * 2, y, to_string(y), GLUT_BITMAP_HELVETICA_12);
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(-angle * 2, 0, 0, 1);

	glLineWidth(2);
	glBegin(GL_LINES);
	{
		glColor3f(0, 0, 0);
		glVertex2f(100, func(100));
		glVertex2f(-100, func(-100));
	}
	glEnd();

	glBegin(GL_LINE_STRIP);
	{
		for (float x = -areaWidth; x <= areaWidth; x += 0.1F)
		{
			glVertex2f(x, pow(x / 4.0, 2));
		}
	}
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	drawString(-areaWidth, areaHeight - 5, "y = " + to_string(a) + " * x", GLUT_BITMAP_HELVETICA_18);

	glFlush();
}

void winReshapeFcn(GLint width, GLint height)
{
	windowWidth = width;
	windowHeight = height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-areaWidth, areaWidth, -areaHeight, areaHeight);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);    // �������� ����
}

void timerFunc(int timerId)
{
	if (timerId == 1)
	{
		if (!pause)
			angle += 0.5;
	}
	glutTimerFunc(17, timerFunc, timerId);
}

void idleFunc()
{
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y)
{
	if (key == VK_SPACE) pause = !pause;
	if (key == VK_BACK) angle = 0;
}

void main(int argc, char** argv)
{
	cout << "Enter line coefficient a:" << endl;
	cin >> a;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_MULTISAMPLE);
	glutInitWindowPosition(300, 0);
	glutInitWindowSize(700, 700);
	glutCreateWindow("����������� ������ #3");
	init();

	glutTimerFunc(17, timerFunc, 1);

	glutIdleFunc(idleFunc);
	glutDisplayFunc(drawShapes);
	glutReshapeFunc(winReshapeFcn);
	glutKeyboardFunc(keyboardFunc);
	glutMainLoop();
}