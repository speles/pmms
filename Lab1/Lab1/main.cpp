#include <Windows.h>
#include <gl\GL.h>
#include "GL\glut.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <string>

using namespace std;

float Y_ARROW_WIDTH = 0.2, Y_ARROW_LENGTH = 0.18;
float X_ARROW_WIDTH = 0.03, X_ARROW_LENGTH = 1;
float areaWidth = X_ARROW_LENGTH + Y_ARROW_WIDTH + M_PI * 2 + 1;
float areaHeight = Y_ARROW_LENGTH + X_ARROW_WIDTH + 1;
int windowWidth, windowHeight;

void init(void)
{
	glClearColor(1, 1, 1, 1);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-areaWidth, areaWidth, -areaHeight, areaHeight);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
}

void drawString(float x, float y, string text, void *font)
{
	glRasterPos2f(x, y);
	for (char c : text)
		glutBitmapCharacter(font, c);

}

void drawShapes(void)
{
	glClearColor(1, 1, 1, 1);

	glColor3b(0, 0, 0);
	glLineWidth(1);
	glBegin(GL_LINES);
	{
		//y axis
		glVertex2f(0, -areaHeight);
		glVertex2f(0, areaHeight);

		//y axis arrow
		glVertex2f(0, areaHeight);
		glVertex2f(-Y_ARROW_WIDTH, areaHeight - Y_ARROW_LENGTH);

		glVertex2f(0, areaHeight);
		glVertex2f(Y_ARROW_WIDTH, areaHeight - Y_ARROW_LENGTH);

		//x axis
		glVertex2f(areaWidth, 0);
		glVertex2f(-areaWidth, 0);

		//x axis arrow
		glVertex2f(areaWidth, 0);
		glVertex2f(areaWidth - X_ARROW_LENGTH, X_ARROW_WIDTH);

		glVertex2f(areaWidth, 0);
		glVertex2f(areaWidth - X_ARROW_LENGTH, -X_ARROW_WIDTH);
	}
	glEnd();

	drawString(areaWidth - X_ARROW_LENGTH, -X_ARROW_WIDTH * 4, "x", GLUT_BITMAP_TIMES_ROMAN_24);

	drawString(-Y_ARROW_WIDTH * 4, areaHeight - Y_ARROW_LENGTH, "y", GLUT_BITMAP_TIMES_ROMAN_24);

	//x axis lines
	for (int x = floor(-M_PI * 2); x <= ceil(M_PI * 2); x++)
	{
		if (x == 0) continue;
		glBegin(GL_LINES);
		{
			glVertex2f(x, X_ARROW_WIDTH);
			glVertex2f(x, -X_ARROW_WIDTH);
		}
		glEnd();

		drawString(x - 0.1, -X_ARROW_WIDTH * 3, to_string(x), GLUT_BITMAP_HELVETICA_12);
	}

	//y axis lines
	for (int y = -1; y <= 1; y++)
	{
		if (y == 0) continue;
		glBegin(GL_LINES);
		glVertex2f(Y_ARROW_WIDTH, y);
		glVertex2f(-Y_ARROW_WIDTH, y);
		glEnd();

		drawString(Y_ARROW_WIDTH * 2, y, to_string(y), GLUT_BITMAP_HELVETICA_12);
	}

	vector<pair<float, float> > points;
	for (float x = -2 * M_PI; x <= 2 * M_PI; x += 0.1F)
		points.push_back(make_pair(x, sin(x)));
	points.push_back(make_pair(2 * M_PI, sin(2 * M_PI)));
	glLineWidth(2);
	glBegin(GL_LINE_STRIP);
	{
		for (int i = 0; i < points.size(); i++)
		{
			glColor3f(max(-points[i].second, 0), min(points[i].second, 1), 0);
			glVertex2f(points[i].first, points[i].second);
		}
	}
	glEnd();

	glFlush();
}

void winReshapeFcn(GLint width, GLint height)
{
	windowWidth = width;
	windowHeight = height;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-areaWidth, areaWidth, -areaHeight, areaHeight);
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);    // �������� ����
}

void main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_MULTISAMPLE);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(500, 500);
	glutCreateWindow("����������� ������ #1");
	init();

	glutDisplayFunc(drawShapes);
	glutReshapeFunc(winReshapeFcn);
	glutMainLoop();
}