#include "Textures.h"

#define GL_CLAMP_TO_EDGE 0x812F

GLuint defaultTexture;

GLuint loadTexture(std::string filename)
{
  //TODO: add type check
  Texture this_texture;
  if (!LoadTGA(&this_texture, const_cast<char*>(filename.c_str())))
  {
    MessageBoxA(NULL, "Something went wrong", "ERROR", MB_OK);
    return 0;
  } else
  {
    glGenTextures(1, &(this_texture.texID));
    glBindTexture(GL_TEXTURE_2D, this_texture.texID);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); //Texture blends with object background
//  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL); //Texture does NOT blend with object background

    /* Select a filtering type. BiLinear filtering produces very good results with little performance impact
      GL_NEAREST               - Basic texture (grainy looking texture)
      GL_LINEAR                - BiLinear filtering
      GL_LINEAR_MIPMAP_NEAREST - Basic mipmapped texture
      GL_LINEAR_MIPMAP_LINEAR  - BiLinear Mipmapped texture
    */

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //only first two can be used

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

//    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_INTERPOLATE);

    if (this_texture.type == GL_RGBA) // Texture can have transparent pixels
    {
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, this_texture.width, this_texture.height, GL_RGBA, GL_UNSIGNED_BYTE, this_texture.imageData);
    } else
    {
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
      gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, this_texture.width, this_texture.height, GL_RGB, GL_UNSIGNED_BYTE, this_texture.imageData);
    }
//  glTexImage2D(GL_TEXTURE_2D, 0, 3, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, pData);  // Use when not wanting mipmaps to be built by openGL
    return this_texture.texID;
  }
}
